'use strict';

module.exports = (args) => {
	return {

		init: [
			{
				type: "shell",
				command: [
					`mkdir -p ${args.path.remote_dir}/{public,private}`,
					`rm -rf ${args.path.remote_dir}/public/cache/*`,
					`rm -rf ${args.path.remote_dir}/public/{api,assets}`,
					`rm -f ${args.path.remote_dir}/public/*.*`,

					`rm -rf ${args.path.remote_dir}/private`,
				]
			},
			{
				type: "copy",
				path: [
					{ src: `public/api` },
					{ src: `public/assets` },
					{ src: `public/*.*`, dest: 'public' },

					{ src: `private` },
				]
			}
		]

	}
};