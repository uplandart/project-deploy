'use strict';
const fs = require('fs-extra-promise');
const path = require('path');
const co = require('co');
const exec = require('child-process-promise').exec;

const node_ssh = require('node-ssh');
const ssh = new node_ssh();

let tasks_key = [];
let ssh_connect = null;

const config = {};

config.connect = {
	host: process.env.HOST,
	username: process.env.USERNAME || process.env.USER,
	privateKey: process.env.PATH_PRIVATE_KEY || process.env.PATH_PK,
	password: process.env.PASSWORD || process.env.PASS,
};
config.path = {
	cwd: path.join(__dirname,'..','..'),
	remote_dir: process.env.PATH_REMOTE
};
config.path.config_file = path.join(config.path.cwd,'project-deploy.config.js');

let tasks = require(config.path.config_file)(config);

for(let i = 2; i < process.argv.length; i++) {
	tasks_key.push(process.argv[i]);
}

co(function* () {

	ssh_connect = yield ssh.connect(config.connect);

	if (tasks_key.length) {
		for (let i = 0; i < tasks_key.length; i++) {
			let task_key = tasks_key[i];
			if (tasks[task_key]) {
				console.log(`Project deploy "${task_key}" start`);
				yield execTask(tasks[task_key]);
				console.log(`Project deploy "${task_key}" done`);
			}
		}
	} else {
		console.log(`Project deploy all start`);
		for (let key in tasks) {
			if (!tasks.hasOwnProperty(key)) continue;
			let task_cfg = tasks[key];
			yield execTask(task_cfg);
		}
		console.log(`Project deploy all done`);
	}

	ssh_connect.dispose();

}).catch(err => {
	console.trace(err);
	ssh_connect.dispose();
});

function* execTask(task_cfg) {
	for (let i = 0; i < task_cfg.length; i++) {
		let task_command = task_cfg[i];
		if (!task_command.hasOwnProperty('remote') || task_command.remote) {
			switch (task_command.type) {
				case 'shell':
					let commands = task_command.command;
					if (!Array.isArray(commands)) commands = [ commands ];
					for (let j = 0; j < commands.length; j++) {
						let res_exec = yield ssh_connect.execCommand(commands[j]);
						if (res_exec.stdout) console.log(res_exec.stdout);//.split('\n');
						if (res_exec.stderr) console.error(res_exec.stderr);//.split('\n');
					}
					break;
				case 'copy':
					let paths = task_command.path;
					if (!Array.isArray(paths)) paths = [ paths ];
					for (let i = 0; i < paths.length; i++) {
						let path_to = paths[i];
						if (!path_to.hasOwnProperty('dest')) path_to.dest = path_to.src;
						path_to.src = path.join(config.path.cwd, path_to.src);
						path_to.dest = path.join(config.path.remote_dir, path_to.dest);

						let command = `scp -r -i ${config.connect.privateKey} ${path_to.src} ${config.connect.username}@${config.connect.host}:${path_to.dest}`;

						let res_exec = yield exec(command);
						if (res_exec.stdout) console.log(res_exec.stdout);//.split('\n');
						if (res_exec.stderr) console.error(res_exec.stderr);//.split('\n');
					}
					break;
			}
		} else {
			switch (task_command.type) {
				case 'shell':
					let commands = task_command.command;
					if (!Array.isArray(commands)) commands = [ commands ];
					for (let j = 0; j < commands.length; j++) {
						let res_exec = yield exec(commands[j]);
						if (res_exec.stdout) console.log(res_exec.stdout);//.split('\n');
						if (res_exec.stderr) console.error(res_exec.stderr);//.split('\n');
					}
					break;
			}
		}
	}
}