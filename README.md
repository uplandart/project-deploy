# Project deploy

Using SCP

## project-deploy.config.js

Example:
```js
module.exports = (args) => {
    return {
        development: [
            {
                type: "shell",
                remote: false,
                command: [
                    `npm run --prefix ${args.path.cwd} build-dev`
                ]
            },
            {
                type: "shell",
                command: [
                    `rm -rf ${args.path.remote_dir}/public`,
                    `rm -rf ${args.path.remote_dir}/private`,
                    `rm -f ${args.path.remote_dir}/server.js`
                ]
            },
            {
                type: "copy",
                path: [
                    { src: `public` },
                    { src: `private` },
                    { src: `server.js`, dest: `` }
                ]
            }
        ]
    }
};
```

## Execution

```
node ./node_modules/project-deploy development
```

with ENV:
* HOST
* USER || USERNAME
* PATH_PRIVATE_KEY || PATH_PK